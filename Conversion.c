//Début du projet le Mardi 18 avril 2023
// J'ai pas mal galéré à tout mettre en place juste pour compiler mon C puis l'executer pour avoir le résulatat dans une console VScode
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>


long int puissance(unsigned long long int x, int n){ //J'ai recodé puissance car en utilisant pow() de math.h y'avait des problemes de types

    long int resultat = 1;
    for (int i = 0; i < n; i++) {
        resultat *= x;
    }
    return resultat;
}

long int conversion_vers_deci(char n[], int base){  // Le code est bon on doit bien veiller à [A,B,..,Z]=[10,11,...,35] et [a,b,...,z]=[36,37,...,55] 
    unsigned long long int result = 0;
    for (long int c = 0; n[c] != '\0'; c++){
        unsigned long long int digit_value = 0;
        if ('0' <= n[c] && n[c] <= '9'){
            digit_value = n[c] - '0';
        }
        else if ('A' <= n[c] && n[c] <= 'Z'){
            digit_value = n[c] - 'A' + 10;
        }
        else if ('a' <= n[c] && n[c] <= 'z')
        {
            digit_value = (n[c] - 'a') + 36;
        }
        result = (result * base )+ digit_value;
    }
    return result;
        
}

char* conversion_vers_base(unsigned long long int n, int base){ //Pour notre fonction opposé on prend en paramètre un ulli car on sait que l'on aura que des nombres décimaux
    int i = 1;
    int p = 1;
    while (p < n){ //i va représenter le nombre de chiffre que n reprensentera dans la base, on fait de l'encadrement en gros
        p = puissance(base, i);
        i++;
    }
    i--;
    unsigned long long int *tableau_coeff = (unsigned long long int*) malloc(i * sizeof(unsigned long long int));
    int k = i;
    unsigned long long int r = n;
    unsigned long long int o = n;
    while(k != 0){
        o = r;
        tableau_coeff[i-k] = r / puissance(base, k-1); // Ici on a a^^i le Coefficient le plus elevé
        r = o % puissance(base, k-1);
        k--;
    }
    char *chaine = (char*) malloc((i+1) * sizeof(char));
    chaine[i] = '\0';
    chaine[0] = '\0';
    for(int j = 0; j < i; j++){

        if (tableau_coeff[j] < 10) {
            chaine[j] = '0' + tableau_coeff[j]; // conversion des chiffres décimaux
        } else if (tableau_coeff[j] < 36) {
            chaine[j] = 'A' + (tableau_coeff[j] - 10); // conversion des chiffres en majuscules
        } else {
            chaine[j] = 'a' + (tableau_coeff[j] - 36); // conversion des chiffres en minuscules
        }
    }
    
    free(tableau_coeff);
    return chaine;
}


int main(){
    char* chaine = conversion_vers_base(126564, 25);
    printf("%s\n", chaine);
    free(chaine);
    printf("%llu", conversion_vers_deci(chaine,25));
    return 0;

}
